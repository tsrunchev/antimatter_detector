#ifdef G4MULTITHREADED
    #include <G4MTRunManager.hh>
    using RunManager = G4MTRunManager;
#else
    #include <G4RunManager.hh>
    using RunManager = G4RunManager;
#endif

#ifdef G4VIS_USE
    #include <G4VisExecutive.hh>
#endif

#ifdef G4UI_USE
    #include <G4UIExecutive.hh>
#endif

#include <G4String.hh>
#include <G4UImanager.hh>
#include <QGSP_BERT.hh>
#include <G4ScoringManager.hh>
#include <G4ScoringBox.hh>
#include <G4StepLimiterPhysics.hh>

#include "DetectorConstruction.hh"
// #include "PhysicsList.hh"
#include "ActionInitialization.hh"
#include "AntiAnalyser.hh"

int main(int argc, char** argv)
{
    std::cout << "Application starting..." << std::endl;

    std::vector<G4String> macros;
    bool interactive = false;

    // Parse command line arguments
    if  (argc == 1)
    {
        interactive = true;
    }
    else
    {
        for (int i = 1; i < argc; i++)
        {
            G4String arg = argv[i];
            if (arg == "-i" || arg == "--interactive")
            {
                interactive = true;
                continue;
            }
            else
            {
                macros.push_back(arg);
            }
        }
    }

    auto runManager = new RunManager();
    runManager->SetVerboseLevel(1);

    #ifdef G4VIS_USE
        G4VisManager* visManager = new G4VisExecutive();
        visManager->Initialize();
    #endif

    // Initialize
    
    G4VModularPhysicsList* physicsList = new QGSP_BERT;
    physicsList->RegisterPhysics(new G4StepLimiterPhysics());
    runManager->SetUserInitialization(physicsList);

    DetectorConstruction* detector = new DetectorConstruction();
    runManager->SetUserInitialization(detector);

    AntiAnalyser* analyser = new AntiAnalyser();
    runManager->SetUserInitialization(new ActionInitialization(analyser,detector));

    #ifdef G4UI_USE
        G4UIExecutive* ui = nullptr;
        if (interactive){ ui = new G4UIExecutive(argc, argv); }
    #endif

    G4UImanager* UImanager = G4UImanager::GetUIpointer();

    UImanager->ApplyCommand("/control/macroPath ./build/macros/");
    #ifndef DEBUG
      UImanager->ApplyCommand("/control/macroPath ./release/macros/");
    #endif
    #ifdef G4UI_USE
        if (interactive)
        {
            if (ui->IsGUI())
                UImanager->ApplyCommand("/control/execute ui.mac");
            else
                UImanager->ApplyCommand("/run/initialize");
        }
    #endif
    for (auto macro : macros)
    {
        G4String command = "/control/execute ";
        UImanager->ApplyCommand(command + macro);
    }

   #ifdef G4UI_USE
        if (interactive)
        {
            ui->SessionStart();
            delete ui;
        }
    #endif

    delete runManager;
    delete analyser;
    std::cout << "Application successfully ended.\nBye :-)" << std::endl;

    return 0;
}