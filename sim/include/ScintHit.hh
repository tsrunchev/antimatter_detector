#ifndef SCINT_HIT_HH
#define SCINT_HIT_HH

#include <G4VHit.hh>
#include <G4THitsCollection.hh>
#include <G4Allocator.hh>
#include <G4ThreeVector.hh>

enum Coordinate {x,y};

class ScintHit : public G4VHit
{
    public:
        // ScintHit();
        ScintHit(G4int layerNum, G4int fiberNum, G4ThreeVector position, G4double energyDep, G4double protonEnergyDep);
        ScintHit(G4int layerNum, G4int fiberNum);
        ~ScintHit();

        G4int GetLayerNum() { return fLayerNum; }
        G4int GetFiberNum() { return fFiberNum; }
        G4ThreeVector GetPosition() { return fPosition;}
        void SetPosition(G4ThreeVector position) { fPosition = position;}
        G4double GetEnergyDep() { return fEnergyDep; }
        void SetEnergyDep(G4double energyDep) { fEnergyDep = energyDep; }
        void AddEnergyDep(G4double energyDep) { fEnergyDep += energyDep; }
        G4double GetProtonEnergyDep() { return fProtonEnergyDep; }
        void SetProtonEnergyDep(G4double protonEnergyDep) { fProtonEnergyDep = protonEnergyDep; }
        void AddProtonEnergyDep(G4double protonEnergyDep) { fProtonEnergyDep += protonEnergyDep; }

        inline void* operator new(size_t);
        inline void operator delete(void *aHit);

        virtual void Print();
        virtual void Draw();
    private:
        G4ThreeVector fPosition;
        G4int fLayerNum;
        G4int fFiberNum;
        G4double fEnergyDep;
        G4double fProtonEnergyDep;

};

typedef G4THitsCollection<ScintHit> ScintHitsCollection;
extern G4ThreadLocal G4Allocator<ScintHit>* ScintHitAllocator;

inline void* ScintHit::operator new(size_t)
{
    if(!ScintHitAllocator){
        ScintHitAllocator = new G4Allocator<ScintHit>;
    }
    return (void*) ScintHitAllocator->MallocSingle();
}

inline void ScintHit::operator delete(void *aHit)
{
    if(!ScintHitAllocator){
        ScintHitAllocator = new G4Allocator<ScintHit>;
    }
    ScintHitAllocator->FreeSingle((ScintHit*) aHit);
}
#endif