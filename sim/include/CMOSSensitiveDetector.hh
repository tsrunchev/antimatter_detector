#ifndef CMOS_SENSITIVE_DETECTOR_HH
#define CMOS_SENSITIVE_DETECTOR_HH

#include <G4VSensitiveDetector.hh>
#include "CMOSHit.hh"

class G4TouchableHistory;
class G4Step;

class CMOSSensitiveDetector : public G4VSensitiveDetector
{
    public:
        explicit CMOSSensitiveDetector(G4String SDname);
        ~CMOSSensitiveDetector();

    protected: // with description
        virtual void Initialize(G4HCofThisEvent* hce);
        virtual G4bool ProcessHits(G4Step*aStep,G4TouchableHistory*ROhist);
        //  The user MUST implement this method for generating hit(s) using the 
        // information of G4Step object. Note that the volume and the position
        // information is kept in PreStepPoint of G4Step.
        //  Be aware that this method is a protected method and it sill be invoked 
        // by Hit() method of Base class after Readout geometry associated to the
        // sensitive detector is handled.
        //  "ROhist" will be given only is a Readout geometry is defined to this
        // sensitive detector. The G4TouchableHistory object of the tracking geometry
        // is stored in the PreStepPoint object of G4Step.
        virtual void EndOfEvent(G4HCofThisEvent* hce);

    private:
        CMOSHitsCollection* fHitsCollection;
        G4int fHCID;

};

#endif