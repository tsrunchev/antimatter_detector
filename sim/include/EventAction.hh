#ifndef EventAction_h
#define EventAction_h 1

#include <G4UserEventAction.hh>

// #include <vector>
// #include <array>

class AntiAnalyser;
class G4Event;

/// Event action
class EventAction : public G4UserEventAction
{
    public:

        EventAction(AntiAnalyser* run_analyser);
        virtual ~EventAction(){};
        
        // virtual void BeginOfEventAction(const G4Event*);
        virtual void EndOfEventAction(const G4Event* event);
        
    private:
        AntiAnalyser* runAnalyser;
        // // hit collections Ids
        // std::array<G4int, kDim> fHodHCID;
        // std::array<G4int, kDim> fDriftHCID;
        // std::array<G4int, kDim> fCalHCID;
        // // histograms Ids
        // std::array<std::array<G4int, kDim>, kDim> fDriftHistoID;
        // // energy deposit in calorimeters cells
        // std::array<std::vector<G4double>, kDim> fCalEdep;
};

#endif