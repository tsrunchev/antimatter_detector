#ifndef CMOS_HIT_HH
#define CMOS_HIT_HH

#include <G4VHit.hh>
#include <G4THitsCollection.hh>
#include <G4Allocator.hh>
#include <G4ThreeVector.hh>


class CMOSHit : public G4VHit
{
    public:
        // CMOSHit();
        CMOSHit(int layer_num, G4ThreeVector& position, G4ThreeVector pre_momentum, G4ThreeVector post_momentum);
        ~CMOSHit();

        void SetPosition(G4ThreeVector xyz) { position = xyz; }
        G4ThreeVector GetPosition() { return position;}

        void SetPreMomentum(G4ThreeVector xyz) { preMomentum = xyz;}
        G4ThreeVector GetPreMomentum() { return preMomentum; }

        void SetPostMomentum(G4ThreeVector xyz) { postMomentum = xyz;}
        G4ThreeVector GetPostMomentum() { return postMomentum; }

        G4int GetLayerNum() { return layerNum; }

        inline void* operator new(size_t);
        inline void operator delete(void *aHit);

        virtual void Print();
        virtual void Draw();
    private:
        G4ThreeVector position;
        G4ThreeVector preMomentum;
        G4ThreeVector postMomentum;
        G4int layerNum;

};

typedef G4THitsCollection<CMOSHit> CMOSHitsCollection;
extern G4ThreadLocal G4Allocator<CMOSHit>* CMOSHitAllocator;

inline void* CMOSHit::operator new(size_t)
{
    if(!CMOSHitAllocator){
        CMOSHitAllocator = new G4Allocator<CMOSHit>;
    }
    return (void*) CMOSHitAllocator->MallocSingle();
}

inline void CMOSHit::operator delete(void *aHit)
{
    if(!CMOSHitAllocator){
        CMOSHitAllocator = new G4Allocator<CMOSHit>;
    }
    CMOSHitAllocator->FreeSingle((CMOSHit*) aHit);
}
#endif