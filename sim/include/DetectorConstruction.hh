#ifndef DETECTOR_CONSTRUCTION_HH
#define DETECTOR_CONSTRUCTION_HH 1

#include <G4VUserDetectorConstruction.hh>
#include <G4SystemOfUnits.hh>
#include <G4ThreeVector.hh>
#include "DetectorMessenger.hh"
#include "CMOSSensitiveDetector.hh"
#include "ScintSensitiveDetector.hh"

class G4LogicalVolume;
/**
  * Obligatory class responsible for geometry - volumes, materials, fields, etc.
  */
class DetectorConstruction : public G4VUserDetectorConstruction
{
public:
  explicit DetectorConstruction(
    G4ThreeVector worldDimensions=G4ThreeVector(2*m,2*m,2*m),
    G4ThreeVector scintSize=G4ThreeVector(64*mm,64*mm,2*mm),
    G4ThreeVector CMOSSize=G4ThreeVector(2*cm,3*cm,30*um),
    G4ThreeVector magneticFieldVec=G4ThreeVector(0,0,-1*tesla), // to be in the same direction as particles in testing
    G4double L_B=10*cm, G4double cmosSpacing=1*cm, G4int scintNumLayers=8) :
      fWorldLog{}, fScintFiberLog{} , fScintLayerLog{}, fCMOSLog{}, fMagneticLogical{},
      fWorldDimensions{worldDimensions}, fScintSize{scintSize},
      fCMOSSize{CMOSSize}, fMagneticFieldVec{magneticFieldVec},
      fL_B{L_B}, fCMOSSpacing{cmosSpacing}, fScintNumLayers{8}, fDetectorMessenger{nullptr},
      fCMOSSD{}, fScintSD{}
      { fDetectorMessenger = new DetectorMessenger(this); };
  ~DetectorConstruction() override;
  G4VPhysicalVolume *Construct() override;
  void ConstructSDandField() override;
  void SetL_B(G4double L_B);
  void SetCmosSpacing(G4double cmos_spacing);

  

// private:
  G4LogicalVolume* fWorldLog;
  G4LogicalVolume* fScintFiberLog;
  G4LogicalVolume* fScintLayerLog;
  G4LogicalVolume* fCMOSLog;
  G4LogicalVolume* fMagneticLogical;

  G4ThreeVector fWorldDimensions;
  G4ThreeVector fScintSize;
  G4ThreeVector fCMOSSize;
  G4ThreeVector fMagneticFieldVec;
  G4double fL_B;
  G4double fCMOSSpacing;
  G4double fScintSpacing=0*mm;
  G4int fScintNumLayers;
  G4int fCMOSNumLayers = 2;
  G4int fScintNumFibersPerLayer = 32;

  // In magnetic field I limit size of step because auxiliaries weren't being drawn
  G4double fmaxStep = 2*mm;

  DetectorMessenger* fDetectorMessenger;

  CMOSSensitiveDetector* fCMOSSD;
  ScintSensitiveDetector* fScintSD;

};

#endif
