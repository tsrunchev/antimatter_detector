

#ifndef DETECTOR_MESSENGER_HH
#define DETECTOR_MESSENGER_HH 1

#include "globals.hh"
#include "G4UImessenger.hh"

class DetectorConstruction;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithoutParameter;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class DetectorMessenger: public G4UImessenger
{
  public:
    explicit DetectorMessenger(DetectorConstruction* );
    virtual ~DetectorMessenger();
    
    virtual void SetNewValue(G4UIcommand*, G4String);
    
  private:
    DetectorConstruction* fDetector;
    
    G4UIdirectory*             fAntiPeterDir;
    G4UIdirectory*             fDetDir;
    G4UIcmdWithADoubleAndUnit* fL_BCmd;
    G4UIcmdWithADoubleAndUnit* fCMOSSpacingCmd; 
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

