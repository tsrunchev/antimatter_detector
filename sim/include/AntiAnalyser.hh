#ifndef AntiAnalyser_h
#define AntiAnalyser_h 1

#include "g4csv.hh"
// #include "g4root.hh"
#include "CMOSHit.hh"
#include "ScintHit.hh"

class AntiAnalyser
{
    public:
        AntiAnalyser();
        ~AntiAnalyser();

        void Create();
        void Save();

        void FillNtuple(CMOSHit* hit);
        void FillNtuple(CMOSHitsCollection* hits_collection);

        void FillNtuple(ScintHit* hit);
        void FillNtuple(ScintHitsCollection* hits_collection);
    private:
        // G4AnalysisManager* analysis_manager;
};

#endif