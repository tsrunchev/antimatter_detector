#ifndef PRIMARY_GENERATOR_ACTION_HH
#define PRIMARY_GENERATOR_ACTION_HH

#include <G4VUserPrimaryGeneratorAction.hh>
#include <G4SystemOfUnits.hh>

#include <iostream>
#include <fstream>

class G4ParticleGun;
class G4GeneralParticleSource;

class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
public:
    PrimaryGeneratorAction();
    ~PrimaryGeneratorAction();
    void GeneratePrimaries(G4Event* anEvent) override;

private:
    G4GeneralParticleSource* fGPS;
    // double x0 = 0,  y0  = 0 , z0  = 146.061*mm;
};

#endif