#ifndef DIMENSIONS_HH
#define DIMENSIONS_HH
#include "G4SystemOfUnits.hh"
#include "G4Types.hh"

namespace dimensions
{
    static constexpr G4double magneticFieldX = 0*tesla;
    static constexpr G4double magneticFieldY = 0*tesla;
    static constexpr G4double magneticFieldZ = -1*tesla;
    static constexpr G4double maxStep = 2*mm;

    static constexpr G4double world_size_X = 2 * m;
    static constexpr G4double world_size_Y = 2 * m;
    static constexpr G4double world_size_Z = 2 * m;
    
    static constexpr G4double L_B = 10*cm;

    static constexpr G4double scint_size_X = 40*mm;
    static constexpr G4double scint_size_Y = 60*mm;
    static constexpr G4double scint_size_Z = 2*mm;
    static constexpr G4double scint_thickness = scint_size_Z;

    static constexpr G4double scint_dist=0*mm;

    static constexpr G4double cmos_size_X = 2*cm;
    static constexpr G4double cmos_size_Y = 3*cm;
    static constexpr G4double cmos_size_Z = 30*um;
    static constexpr G4double cmos_thickness = cmos_size_Z;

    static constexpr G4double cmos_dist = L_B/10;

    static constexpr G4int scintNumLayers = 8;
    static constexpr G4int cmosNumLayers = 2;

    static constexpr G4double source_Dist = 0.5*cm;
    static constexpr G4double source_x = 0;
    static constexpr G4double source_y = 0;
    static constexpr G4double source_z = L_B +
                        cmosNumLayers*cmos_thickness + cmos_dist*(cmosNumLayers-1) +
                        scintNumLayers*scint_thickness + scint_dist*(scintNumLayers-1) +
                        source_Dist;
    static constexpr G4bool cmosSource = false;

    static const G4double GetCmosZ(G4int cmosIndex){
        auto cmos0 = L_B/2 + cmos_dist*(cmosNumLayers-1) + cmos_thickness*(cmosNumLayers-0.5);
        if (cmosIndex>=cmosNumLayers)
        {
            cmos0 = cmos0 - L_B + cmos_dist;
        }
        return cmos0 - (cmos_thickness + cmos_dist)*cmosIndex;
    }

}

#endif