#ifndef ACTION_INITIALIZATION_HH
#define ACTION_INITIALIZATION_HH

#include <G4VUserActionInitialization.hh>

class AntiAnalyser;
class DetectorConstruction;

class ActionInitialization : public G4VUserActionInitialization
{
public:
    ActionInitialization(AntiAnalyser* run_analyser, DetectorConstruction* detector);
    ~ActionInitialization() override;

    void Build() const override;

    void BuildForMaster() const override;   

    AntiAnalyser* fRunAnalyser;
    DetectorConstruction* fDetector;
};

#endif