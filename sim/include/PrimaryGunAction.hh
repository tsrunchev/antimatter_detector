#ifndef PRIMARY_GENERATOR_ACTION_HH
#define PRIMARY_GENERATOR_ACTION_HH


#include <G4VUserPrimaryGeneratorAction.hh>
#include <G4SystemOfUnits.hh>
#include <G4Types.hh>

#include <iostream>
#include <fstream>

class G4ParticleGun;
class G4GeneralParticleSource;
class DetectorConstruction;

class PrimaryGunAction : public G4VUserPrimaryGeneratorAction
{
public:
    PrimaryGunAction(DetectorConstruction* detector);
    ~PrimaryGunAction();
    void GeneratePrimaries(G4Event* anEvent) override;
    G4double GetCmosZ(G4int cmosIndex);

private:
    G4ParticleGun* fGun;
    // std::ofstream myFile;
    G4GeneralParticleSource* fGPS;
    // double x0 = 0,  y0  = 0 , z0  = 146.061*mm;
    DetectorConstruction* fDetector;
};

#endif