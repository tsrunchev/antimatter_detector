#include <G4ThreeVector.hh>

#include "ScintHit.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4VisAttributes.hh"


G4ThreadLocal G4Allocator<ScintHit>* ScintHitAllocator;

ScintHit::ScintHit(G4int layerNum, G4int fiberNum, G4ThreeVector position, G4double energyDep, G4double protonEnergyDep)
    : fLayerNum{layerNum},fFiberNum{fiberNum},fPosition{position},fEnergyDep{energyDep},fProtonEnergyDep{protonEnergyDep}
{
}

ScintHit::ScintHit(G4int layerNum, G4int fiberNum)
    : fLayerNum{layerNum},fFiberNum{fiberNum},fPosition{},fEnergyDep{.0},fProtonEnergyDep{.0}
{
    // G4cout<<G4endl<<fFiberNum<<G4endl;
}

ScintHit::~ScintHit()
{
}

void ScintHit::Print()
{
    if (fEnergyDep>0)
    {
        G4cout<<"Scint layer: "<<fLayerNum<<", fiber: "<<fFiberNum<<" hit."<<G4endl<<
            "    Position: ("<<fPosition.x()<<","<<fPosition.y()<<","<<fPosition.z()<<"),"<<G4endl<<
            "    Energy deposited: "<<fEnergyDep<<G4endl<<
            "    Proton/Antiproton energy deposited: "<<fProtonEnergyDep<<G4endl;
    }
}

void ScintHit::Draw()
{
    auto visManager = G4VVisManager::GetConcreteInstance();
    if (! visManager) return;

    G4Circle circle{fPosition};
    circle.SetScreenSize(2);
    circle.SetFillStyle(G4Circle::filled);
    G4Colour colour(0.,1.,0.3);
    G4VisAttributes attribs(colour);
    circle.SetVisAttributes(attribs);
//   visManager->Draw(circle);
}