
#include "DetectorMessenger.hh"

#include "DetectorConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorMessenger::DetectorMessenger( DetectorConstruction* Det)
: G4UImessenger(),
 fDetector(Det),
 fAntiPeterDir(nullptr),
 fDetDir(nullptr),
 fL_BCmd(nullptr),
 fCMOSSpacingCmd(nullptr)
{ 
  fAntiPeterDir = new G4UIdirectory("/AntiPeter/");
  fAntiPeterDir->SetGuidance("UI commands of this example");
  
  G4bool broadcast = false;
  fDetDir = new G4UIdirectory("/AntiPeter/det/",broadcast);
  fDetDir->SetGuidance("detector control");
    
  fL_BCmd = new G4UIcmdWithADoubleAndUnit("/AntiPeter/det/setL_B",this);
  fL_BCmd->SetGuidance("Set Thickness of the magnetic field");
  fL_BCmd->SetParameterName("Size",false);
  fL_BCmd->SetRange("Size>=0.");
  fL_BCmd->SetUnitCategory("Length");
  fL_BCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  fCMOSSpacingCmd = new G4UIcmdWithADoubleAndUnit("/AntiPeter/det/setCMOSSpacing",this);
  fCMOSSpacingCmd->SetGuidance("Set z-Distance between CMOS chips");
  fCMOSSpacingCmd->SetParameterName("Size",false);
  fCMOSSpacingCmd->SetRange("Size>=0.");
  fCMOSSpacingCmd->SetUnitCategory("Length");  
  fCMOSSpacingCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorMessenger::~DetectorMessenger()
{
  delete fL_BCmd;
  delete fCMOSSpacingCmd;
  delete fDetDir;
  delete fAntiPeterDir;  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == fL_BCmd )
   { fDetector->SetL_B(fL_BCmd->GetNewDoubleValue(newValue));}
   
  if( command == fCMOSSpacingCmd )
   { fDetector->SetCmosSpacing(fCMOSSpacingCmd->GetNewDoubleValue(newValue));}
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
