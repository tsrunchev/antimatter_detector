#include "PrimaryGunAction.hh"
#include "DetectorConstruction.hh"

#include <G4ParticleTable.hh>
#include <G4Event.hh>
#include <G4SystemOfUnits.hh>
#include <G4ParticleGun.hh>
#include <Randomize.hh>
#include <G4GeneralParticleSource.hh>
#include <G4AntiProton.hh>
#include <G4Proton.hh>
#include <G4RunManager.hh>

#include <iostream>

using namespace std;
// using namespace dimensions;

// PrimaryGunAction::PrimaryGunAction():fGun{},fGPS{}//,myFile{}
PrimaryGunAction::PrimaryGunAction(DetectorConstruction* detector):fGun{},fGPS{},fDetector{detector}//,myFile{}
{
    fGun = new G4ParticleGun();
    // fGun->SetParticleDefinition(G4AntiProton::AntiProtonDefinition());
    fGun->SetParticleDefinition(G4Proton::ProtonDefinition());
    fGun->SetParticleMomentum(290. * MeV); 
    fGun->SetParticleMomentumDirection(G4ThreeVector(0., 0., -1.));

    fGun->SetParticlePosition({0, 0, 286.061*mm});

}

PrimaryGunAction::~PrimaryGunAction()
{
    delete fGun;
    delete fGPS;
    // delete fDetector;
    // myFile.close();
}

void PrimaryGunAction::GeneratePrimaries(G4Event* anEvent)
{
    // auto fDetector = (DetectorConstruction*)G4RunManager::GetRunManager()->GetUserDetectorConstruction();
    // Get Center of last CMOS. Need to fix if XY change
    G4ThreeVector last_cmos_center = G4ThreeVector(0, 0, GetCmosZ(fDetector->fCMOSNumLayers * 2 - 1));
    G4cout << "cmosz3 = " << GetCmosZ(fDetector->fCMOSNumLayers * 2 - 1) << " cmosz0 = " << GetCmosZ(0) << G4endl;
    // if (cmosSource)
    // {
        G4double source_x_new = G4UniformRand()*fDetector->fCMOSSize.x() - fDetector->fCMOSSize.x()/2;
        G4double source_y_new = G4UniformRand()*fDetector->fCMOSSize.y() - fDetector->fCMOSSize.y()/2;
        G4double source_z_new = GetCmosZ(0) + fDetector->fCMOSSize.z(); // this puts it half a CMOS chip thickness outside the cmos
        fGun->SetParticlePosition(G4ThreeVector(source_x_new, source_y_new, source_z_new));
    // }

    //Get vector from particle to CMOS Center
    auto particle_pos = fGun->GetParticlePosition(); // Predefined?
    G4ThreeVector particle_dir = last_cmos_center - particle_pos;
    
    // Set XY of direction vector to a random value within the CMOS
    particle_dir.setX(particle_dir.getX() + (fDetector->fCMOSSize.x()/2)*(G4UniformRand()*2-1)*0.9);
    particle_dir.setY(particle_dir.getY() + (fDetector->fCMOSSize.y()/2)*(G4UniformRand()*2-1)*0.9);

//    G4cout<< "Random direction: x=" << particle_dir.getX()  << ", y=" << particle_dir.getY() << ", z=" << particle_dir.getZ() <<G4endl;
    fGun->SetParticleMomentumDirection(particle_dir);
    
    fGun->GeneratePrimaryVertex(anEvent);
    // fGPS->GeneratePrimaryVertex(anEvent);
}

G4double PrimaryGunAction::GetCmosZ(G4int cmosIndex)
{
    auto cmos0 = fDetector->fL_B/2 + fDetector->fCMOSSpacing*(fDetector->fCMOSNumLayers - 1) +
                    fDetector->fCMOSSize.z()*(fDetector->fCMOSNumLayers - 0.5);
    if (cmosIndex>=fDetector->fCMOSNumLayers)
    {
        cmos0 = cmos0 - fDetector->fL_B + fDetector->fCMOSSpacing;
    }
    auto result = cmos0 - (fDetector->fCMOSSize.z() + fDetector->fCMOSSpacing)*cmosIndex;
    return result;
}