#include <G4ThreeVector.hh>

#include "CMOSHit.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4VisAttributes.hh"


G4ThreadLocal G4Allocator<CMOSHit>* CMOSHitAllocator;

CMOSHit::CMOSHit(int layer_num, G4ThreeVector& position, G4ThreeVector pre_momentum, G4ThreeVector post_momentum)
    : layerNum{layer_num},position{position},preMomentum{pre_momentum},postMomentum{post_momentum}
{

}

CMOSHit::~CMOSHit()
{
}

void CMOSHit::Print()
{
    G4cout<<"CMOS"<<layerNum<<" hit,"<<G4endl<<"    Position ("<<
        position.x()<<","<<position.y()<<","<<position.z()<<"),"<<G4endl<<"    Pre-Momentum ("<<
        preMomentum.x()<<","<<preMomentum.y()<<","<<preMomentum.z()<<")"<<"    Post-Momentum ("<<
        postMomentum.x()<<","<<postMomentum.y()<<","<<postMomentum.z()<<")"<<G4endl;
}

void CMOSHit::Draw()
{
    auto visManager = G4VVisManager::GetConcreteInstance();
    if (! visManager) return;

    G4Circle circle{position};
    circle.SetScreenSize(2);
    circle.SetFillStyle(G4Circle::filled);
    G4Colour colour(0.,1.,0.3);
    G4VisAttributes attribs(colour);
    circle.SetVisAttributes(attribs);
  // visManager->Draw(circle);
}