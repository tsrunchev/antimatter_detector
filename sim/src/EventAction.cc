#include "EventAction.hh"

#include "G4Event.hh"
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"

#include "AntiAnalyser.hh"
#include "CMOSHit.hh"
#include "ScintHit.hh"

EventAction::EventAction(AntiAnalyser* run_analyser)
: G4UserEventAction{}
{
    runAnalyser = run_analyser;
}

void EventAction::EndOfEventAction(const G4Event* event)
{
    G4HCofThisEvent* hcof = event->GetHCofThisEvent();
    CMOSHitsCollection* cmos_HC = (CMOSHitsCollection*)hcof->GetHC(G4SDManager::GetSDMpointer()->GetCollectionID("CMOSHitCollection"));
    // CMOSHitsCollection* cmos_HC = (CMOSHitsCollection*)hcof->GetHC(G4SDManager::GetSDMpointer()->GetCollectionID("CMOSHitCollection"));
    // CMOSHitsCollection* cmos_HC = (CMOSHitsCollection*)hcof->GetHC(G4SDManager::GetSDMpointer()->GetCollectionID("CMOSHitCollection"));
    if (cmos_HC)
        runAnalyser->FillNtuple(cmos_HC);
    // G4cout<< "NUMBER OF COLLECTIONS " << hcof->GetNumberOfCollections() << " hit collection id " << G4SDManager::GetSDMpointer()->GetCollectionID("CMOSHitCollection") << G4endl;
    // for (int i=0;i<4;i++)
    //     G4cout<<hcof->GetHC(i)->GetName()<<G4endl;
    ScintHitsCollection* scint_HC = (ScintHitsCollection*)hcof->GetHC(G4SDManager::GetSDMpointer()->GetCollectionID("ScintHitCollection"));
    if (scint_HC)
        runAnalyser->FillNtuple(scint_HC);
}