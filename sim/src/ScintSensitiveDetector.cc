#include "ScintSensitiveDetector.hh"

#include "G4StepPoint.hh"
#include "G4SDManager.hh"
#include "G4AntiProton.hh"
#include "G4Proton.hh"


ScintSensitiveDetector::ScintSensitiveDetector(const G4String SDname, G4int numLayers, G4int fibersPerLayer)
    : G4VSensitiveDetector(SDname), fNumLayers{numLayers}, fFibersPerLayer{fibersPerLayer},
      fHitsCollection{},fHCID{}
{
    G4cout<<"### Creating SD with name: "<<SDname<<G4endl;
    collectionName.insert("ScintHitCollection");
}



ScintSensitiveDetector::~ScintSensitiveDetector()
{
//    delete fHitsCollection;
}

void ScintSensitiveDetector::Initialize(G4HCofThisEvent* hce)
{
    fHitsCollection = new ScintHitsCollection(SensitiveDetectorName, collectionName[0]);

    if (fHCID<=0) { 
        fHCID = G4SDManager::GetSDMpointer()->GetCollectionID(fHitsCollection); 
    }
    hce->AddHitsCollection(fHCID,fHitsCollection);
    for (int i = 0; i < 2*fNumLayers; i++) // Because fNumLayers is on each side
    {
        for (int j = 0; j < fFibersPerLayer; j++)
        {
            fHitsCollection->insert(new ScintHit(i,j));
        }
        
    }

}

void ScintSensitiveDetector::EndOfEvent(G4HCofThisEvent*)
{
    G4cout<<"SDName "<<GetName()<<" and "<<GetFullPathName()<<G4endl;
    // fHitsCollection->PrintAllHits();
    G4cout<<"Collection Name "<<fHitsCollection->GetName()<<  " CollectionID " << fHitsCollection->GetColID()<<G4endl;
}

G4bool ScintSensitiveDetector::ProcessHits(G4Step*aStep,G4TouchableHistory*ROhist)
{
    G4double total_energy_deposit = aStep->GetTotalEnergyDeposit();
    if (total_energy_deposit==0.0) return true;

    G4StepPoint* pre_step_point = aStep->GetPreStepPoint();
    // G4StepPoint* post_step_point = aStep->GetPostStepPoint();
    
    auto touchable = pre_step_point->GetTouchable();
    G4int scint_fiber = touchable->GetCopyNumber();
    G4int scint_layer = touchable->GetCopyNumber(1);
    // auto rotation = touchable->GetRotation(1);
    // G4ThreeVector pos;
    
    // if (rotation->xy()!=0.0)
    // {
    //     // apparently this method returns a coordinate in global coordinate system, I thought it would be local
    //     pos = G4ThreeVector(touchable->GetTranslation(0).x(), 0, touchable->GetTranslation(1).z());
    // }
    // else
    // {
    //     pos = G4ThreeVector(0, touchable->GetTranslation(0).y(), touchable->GetTranslation(1).z());
    // }
    G4ThreeVector pos = G4ThreeVector(touchable->GetTranslation(0).x(), touchable->GetTranslation(0).y(), touchable->GetTranslation(1).z());
    
    // G4cout<<"\n ######## TRACK ID "<<aStep->GetTrack()->GetTrackID()<<" ########\n";
    
    // ScintHit* hit = new ScintHit(scint_layer, scint_fiber, pos,);
    // auto a = fHitsCollection->GetSize();
    ScintHit* hit = (ScintHit*)fHitsCollection->GetHit(fFibersPerLayer*scint_layer + scint_fiber);

    hit->AddEnergyDep(total_energy_deposit);
    if ((aStep->GetTrack()->GetDefinition() == G4AntiProton::AntiProtonDefinition()) || 
        (aStep->GetTrack()->GetDefinition() == G4Proton::ProtonDefinition())) 
        {
            hit->AddProtonEnergyDep(total_energy_deposit);
        }

    hit->SetPosition(pos);
    return true;
}