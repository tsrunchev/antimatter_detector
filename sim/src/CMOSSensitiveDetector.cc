#include "CMOSSensitiveDetector.hh"

#include "G4StepPoint.hh"
#include "G4SDManager.hh"
#include "G4AntiProton.hh"
#include "G4Proton.hh"


CMOSSensitiveDetector::CMOSSensitiveDetector(const G4String SDname)
    : G4VSensitiveDetector(SDname), fHitsCollection{}, fHCID{}
{
    G4cout<<"### Creating SD with name: "<<SDname<<G4endl;
    collectionName.insert("CMOSHitCollection");
}



CMOSSensitiveDetector::~CMOSSensitiveDetector()
{
//    delete fHitsCollection;
}

void CMOSSensitiveDetector::Initialize(G4HCofThisEvent* hce)
{
    fHitsCollection = new CMOSHitsCollection(SensitiveDetectorName, collectionName[0]);

    if (fHCID<=0) { 
        fHCID = G4SDManager::GetSDMpointer()->GetCollectionID(fHitsCollection); 
    }
    hce->AddHitsCollection(fHCID,fHitsCollection);
}

void CMOSSensitiveDetector::EndOfEvent(G4HCofThisEvent*)
{
    G4cout<<"SDName "<<GetName()<<" and "<<GetFullPathName()<<G4endl;
    fHitsCollection->PrintAllHits();
    G4cout<<"Collection Name "<<fHitsCollection->GetName()<<  " CollectionID " << fHitsCollection->GetColID()<<G4endl;
    
}

G4bool CMOSSensitiveDetector::ProcessHits(G4Step*aStep,G4TouchableHistory*ROhist)
{
    if ((aStep->GetTrack()->GetDefinition() != G4AntiProton::AntiProtonDefinition()) && 
        (aStep->GetTrack()->GetDefinition() != G4Proton::ProtonDefinition())) { return false; }
    
    G4StepPoint* pre_step_point = aStep->GetPreStepPoint();
    G4StepPoint* post_step_point = aStep->GetPostStepPoint();
    
    G4int cmos_layer = pre_step_point->GetTouchable()->GetCopyNumber();
    G4ThreeVector position = (pre_step_point->GetPosition() + post_step_point->GetPosition())/2;
    G4ThreeVector pre_momentum = pre_step_point->GetMomentum();
    G4ThreeVector post_momentum = post_step_point->GetMomentum();
    // G4cout<<"\n ######## TRACK ID "<<aStep->GetTrack()->GetTrackID()<<" ########\n";
    
    CMOSHit* hit = new CMOSHit(cmos_layer, position, pre_momentum, post_momentum);
    fHitsCollection->insert(hit);

    return true;
}