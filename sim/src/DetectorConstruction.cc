#include "DetectorConstruction.hh"
#include "CMOSSensitiveDetector.hh"
#include "ScintSensitiveDetector.hh"
// #include "Dimensions.hh"

#include <G4PhysicalConstants.hh>
#include <G4LogicalVolume.hh>
#include <G4PVPlacement.hh>
#include <G4NistManager.hh>
#include <G4SystemOfUnits.hh>
#include <G4VisAttributes.hh>
#include <G4Box.hh>
#include <G4Colour.hh>
#include <G4UniformMagField.hh>
#include <G4FieldManager.hh>
#include <G4SDManager.hh>
#include <G4UserLimits.hh>
#include <G4PVReplica.hh>
#include <G4RunManager.hh>

#include <sstream>

using namespace std;
// using namespace dimensions;

DetectorConstruction::~DetectorConstruction()
{
  delete fWorldLog,fScintFiberLog,fScintLayerLog,fCMOSLog,fMagneticLogical,
            fDetectorMessenger,fCMOSSD,fScintSD;
}

G4VPhysicalVolume * DetectorConstruction::Construct()
{
  G4NistManager* nist = G4NistManager::Instance();

  // World Volume

    // 1) Solid
    G4VSolid* world_box = new G4Box("world", fWorldDimensions.x()/2, fWorldDimensions.y()/2, fWorldDimensions.z()/2);
    // 2) Logical volume
    G4double density = universe_mean_density; //from PhysicalConstants.h
    G4double pressure = 1.e-19*pascal;
    G4double temperature = 0.1*kelvin;
    auto world_material = new G4Material("Galactic", 1., 1.01*g/mole, universe_mean_density, kStateGas,temperature,pressure);
    fWorldLog = new G4LogicalVolume(world_box, world_material, "world");
    G4VisAttributes* vis_attr = new G4VisAttributes();
    vis_attr->SetVisibility(false);
    fWorldLog->SetVisAttributes(vis_attr);

    // 3) Physical volume
    G4VPhysicalVolume* world_phys = new G4PVPlacement(nullptr, {}, fWorldLog, "world", nullptr, false, 0);


  // Magnetic field volume
    G4VSolid* mag_box = new G4Box("magnetic_volume", fScintSize.x()/2, fScintSize.y()/2, fL_B/2);
    fMagneticLogical = new G4LogicalVolume(mag_box, world_material, "magnetic_volume");
    auto mag_attr = new G4VisAttributes(new G4Colour(1,0,0,0.1));
    mag_attr->SetVisibility(true);
    fMagneticLogical->SetVisAttributes(mag_attr);
    new G4PVPlacement(nullptr, G4ThreeVector(0,0,0), fMagneticLogical, "magnetic_volume", fWorldLog, 0, 0);
  // CMOS
    // Material and Logical volume
      G4Material* cmos_mat = nist->FindOrBuildMaterial("G4_Si");
      G4VSolid* cmos_box = new G4Box("cmos", fCMOSSize.x()/2, fCMOSSize.y()/2, fCMOSSize.z()/2);
      fCMOSLog = new G4LogicalVolume(cmos_box, cmos_mat, "cmos");

      G4VisAttributes* yellow_attr = new G4VisAttributes();
      yellow_attr->SetColour(1,1,0, 0.8 / fCMOSNumLayers);
      yellow_attr->SetVisibility(true);
      yellow_attr->SetForceSolid(true);
      fCMOSLog->SetVisAttributes(yellow_attr);

    // Position
      // numbering so particle from positive side enters 0-n layers
      G4double min_Z = fL_B/2 + fCMOSSize.z()/2;
      for (int i = 0; i < fCMOSNumLayers; i++)
      {
        ostringstream pos_name; pos_name << "cmos" << i;
        G4ThreeVector position = {0, 0, min_Z + i*(fCMOSSize.z() + fCMOSSpacing)};
        new G4PVPlacement(nullptr, position, fCMOSLog, pos_name.str(), fWorldLog, 0, fCMOSNumLayers - 1 - i);
        ostringstream neg_name; neg_name << "cmos" << i + fCMOSNumLayers;
        new G4PVPlacement(nullptr, -position, fCMOSLog, neg_name.str(), fWorldLog, 0, i + fCMOSNumLayers);
      }

  // Scintillating Polystyrene layers

    // Material and Logical volume
      G4VSolid* scint_layer_box = new G4Box("scintillator_layer", fScintSize.x()/2, fScintSize.y()/2, fScintSize.z()/2);
      G4Material* scint_mat =  nist->FindOrBuildMaterial("G4_POLYSTYRENE");
      fScintLayerLog = new G4LogicalVolume(scint_layer_box, world_material, "scintillator_layer");

      G4VSolid* fiber_box = new G4Box("scint_fiber",fScintSize.x()/2,fScintSize.z()/2,fScintSize.z()/2);
      fScintFiberLog = new G4LogicalVolume(fiber_box, scint_mat, "scint_fiber");
      // new G4PVReplica("scint_fiber_replica", fiber_log, fScintLayerLog, kZAxis, fScintNumFibersPerLayer, fScintSize.z());
      for (int i = 0; i < fScintNumFibersPerLayer; i++)
      {
        ostringstream pos_name; pos_name << "scint_fiber" << i;
        G4ThreeVector position = {0, (i + 0.5 - fScintNumFibersPerLayer / 2) * fScintSize.z(), 0};
        new G4PVPlacement(nullptr, position, fScintFiberLog, pos_name.str(), fScintLayerLog, 0, i);
      }


      G4VisAttributes* green_attr = new G4VisAttributes();
      green_attr->SetColour(0,1,0,0.8/fScintNumLayers);
      green_attr->SetVisibility(true);
      green_attr->SetForceSolid(true);
      fScintLayerLog->SetVisAttributes(green_attr);
      fScintFiberLog->SetVisAttributes(green_attr);

    // Placement
      // numbering so particle from positive side enters 0-n layers
      min_Z = fL_B/2 + fCMOSSize.z()*fCMOSNumLayers + fCMOSSpacing*(fCMOSNumLayers-1) + fScintSize.z()/2;
      G4RotationMatrix* rotationMatrix = new G4RotationMatrix();
      rotationMatrix->rotateZ(90.*deg);

      G4RotationMatrix* temp_rotation_matrix = nullptr;
      for (int i = 0; i < fScintNumLayers; i++)
      {
        if (i%2==0)
          temp_rotation_matrix = rotationMatrix;
        else
          temp_rotation_matrix = nullptr;
        ostringstream pos_name; pos_name << "scintillator" << i;
        G4ThreeVector position = {0, 0, min_Z + i*(fScintSize.z() + fScintSpacing)};
        new G4PVPlacement(temp_rotation_matrix, position, fScintLayerLog, pos_name.str(), fWorldLog, 0, fScintNumLayers-1-i);
        ostringstream neg_name; neg_name << "scintillator" << i+fScintNumLayers;
        new G4PVPlacement(temp_rotation_matrix, -position, fScintLayerLog, neg_name.str(), fWorldLog, 0, i+fScintNumLayers);
      }

  // The Construct() method has to return the final (physical) world volume:
  return world_phys;
}

void DetectorConstruction::ConstructSDandField()
{
  G4cout<<G4endl<<G4endl<<G4endl<<"CONSTRUCTING SDANDFIELD"<<G4endl<<G4endl<<G4endl;
  
  G4UniformMagField* fMagneticField = new G4UniformMagField(
        fMagneticFieldVec);
  G4FieldManager* fFieldMgr = new G4FieldManager();
  fFieldMgr->SetDetectorField(fMagneticField);
  fFieldMgr->CreateChordFinder(fMagneticField);
  G4bool forceToAllDaughters = true;
  fMagneticLogical->SetFieldManager(fFieldMgr, forceToAllDaughters);

  G4UserLimits *fStepLimit = new G4UserLimits(fmaxStep);
  fMagneticLogical->SetUserLimits(fStepLimit);

  G4SDManager* sdManager = G4SDManager::GetSDMpointer();

  if (!fCMOSSD)
    fCMOSSD = new CMOSSensitiveDetector("CMOS");
  sdManager->AddNewDetector(fCMOSSD);
  SetSensitiveDetector(fCMOSLog, fCMOSSD);

  if (!fScintSD)
    fScintSD = new ScintSensitiveDetector("Scint", fScintNumLayers, fScintNumFibersPerLayer);
  sdManager->AddNewDetector(fScintSD);
  SetSensitiveDetector(fScintFiberLog,fScintSD);

}

void DetectorConstruction::SetL_B(G4double L_B)
{ 
  fL_B = L_B;  
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}

void DetectorConstruction::SetCmosSpacing(G4double cmos_spacing)
{ 
  fCMOSSpacing = cmos_spacing; 
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}

