#include "AntiAnalyser.hh"

#include <chrono>
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"

#include "DetectorConstruction.hh"

AntiAnalyser::AntiAnalyser(){
    
    // analysis_manager = G4AnalysisManager::Instance();
};
AntiAnalyser::~AntiAnalyser(){};

void AntiAnalyser::Create()
{
    auto analysis_manager = G4AnalysisManager::Instance();
    analysis_manager->SetNtupleDirectoryName("ntuple");

    analysis_manager->CreateNtuple("CMOS", "CMOS");
    analysis_manager->CreateNtupleIColumn("CMOSLayer"); // column Id = 0
    analysis_manager->CreateNtupleDColumn("PosX[mm]"); // column Id = 1
    analysis_manager->CreateNtupleDColumn("PosY[mm]"); // column Id = 2
    analysis_manager->CreateNtupleDColumn("PosZ[mm]"); // column Id = 3
    analysis_manager->CreateNtupleDColumn("PreMomX[MeV/c]"); // column Id = 4
    analysis_manager->CreateNtupleDColumn("PreMomY[MeV/c]"); // column Id = 5
    analysis_manager->CreateNtupleDColumn("PreMomZ[MeV/c]");  // column Id = 6
    analysis_manager->CreateNtupleDColumn("PostMomX[MeV/c]"); // column Id = 7
    analysis_manager->CreateNtupleDColumn("PostMomY[MeV/c]"); // column Id = 8
    analysis_manager->CreateNtupleDColumn("PostMomZ[MeV/c]");  // column Id = 9

    analysis_manager->FinishNtuple();

    analysis_manager->CreateNtuple("Scint", "Scint");
    analysis_manager->CreateNtupleIColumn("ScintLayer"); // column Id = 0
    analysis_manager->CreateNtupleIColumn("ScintFiber"); // column Id = 1
    analysis_manager->CreateNtupleDColumn("PosX[mm]"); // column Id = 2
    analysis_manager->CreateNtupleDColumn("PosY[mm]"); // column Id = 3
    analysis_manager->CreateNtupleDColumn("PosZ[mm]"); // column Id = 4
    analysis_manager->CreateNtupleDColumn("EDep[MeV]"); // column Id = 5
    analysis_manager->CreateNtupleDColumn("p+-EDep[MeV]"); // column Id = 6
    analysis_manager->FinishNtuple();

    auto result = std::chrono::system_clock::now().time_since_epoch().count(); //filename is time since epoch in milliseconds (per run)
    auto runManager = G4RunManager::GetRunManager();
    auto cmosSpacing = ((DetectorConstruction*)runManager->GetUserDetectorConstruction())->fCMOSSpacing;
    std::ostringstream cmos_file_name; cmos_file_name << "ntuple/" << result << "_cmosSpacing_" << cmosSpacing/mm << "mm";
    G4bool fileOpen = analysis_manager->OpenFile(cmos_file_name.str());
    if (! fileOpen) 
    {
        G4cerr << "\n---> AntiAnalyser::Create(): cannot open " 
            << analysis_manager->GetFileName() << G4endl;
        return;
    }

    // G4cout<<"########### first ntuple columnid at creation "<<analysis_manager->GetFirstNtupleColumnId()<<G4endl;
    
    G4cout << "\n----> Output file is open in " 
        << analysis_manager->GetFileName() << "." 
        << analysis_manager->GetFileType() << G4endl;

    

}


void AntiAnalyser::Save()
{
    auto analysis_manager = G4AnalysisManager::Instance();
    analysis_manager->Write();
    analysis_manager->CloseFile(); 
    
    G4cout << "\n----> Ntuples are saved\n" << G4endl;
        
    delete G4AnalysisManager::Instance();
}

// Fill CMOS ntuple with a single hit
void AntiAnalyser::FillNtuple(CMOSHit* hit)
{
    auto analysis_manager = G4AnalysisManager::Instance();
    G4cout<<"########### filling hit on CMOS"<<hit->GetLayerNum()<<G4endl;
    
    analysis_manager->FillNtupleIColumn(0,0,hit->GetLayerNum());
    analysis_manager->FillNtupleDColumn(0,1,hit->GetPosition().x()/mm);
    analysis_manager->FillNtupleDColumn(0,2,hit->GetPosition().y()/mm);
    analysis_manager->FillNtupleDColumn(0,3,hit->GetPosition().z()/mm);
    analysis_manager->FillNtupleDColumn(0,4,hit->GetPreMomentum().x()/MeV);
    analysis_manager->FillNtupleDColumn(0,5,hit->GetPreMomentum().y()/MeV);
    analysis_manager->FillNtupleDColumn(0,6,hit->GetPreMomentum().z()/MeV);
    analysis_manager->FillNtupleDColumn(0,7,hit->GetPostMomentum().x()/MeV);
    analysis_manager->FillNtupleDColumn(0,8,hit->GetPostMomentum().y()/MeV);
    analysis_manager->FillNtupleDColumn(0,9,hit->GetPostMomentum().z()/MeV);

    analysis_manager->AddNtupleRow(0);
}

// Fill CMOS ntuple with a collection
void AntiAnalyser::FillNtuple(CMOSHitsCollection* hits_collection)
{
    size_t nhit = hits_collection->GetSize();
    if (nhit<4)
    {
        G4cout<<"####### ONLY " << nhit << " CMOS hits, not filling" << G4endl;
        return;
    }
    for (unsigned long i = 0; i < nhit; ++i) {
        CMOSHit* hit = static_cast<CMOSHit*>(hits_collection->GetHit(i));
        FillNtuple(hit);
    }
}

// Fill scintillator ntuple with a single hit
void AntiAnalyser::FillNtuple(ScintHit* hit)
{
    if (hit->GetEnergyDep()==0) return;

    auto analysis_manager = G4AnalysisManager::Instance();
    // G4cout<<"########### filling hit on Scint layer "<<hit->GetLayerNum()<<" fiber "<<hit->GetFiberNum()<<G4endl;
    
    analysis_manager->FillNtupleIColumn(1,0,hit->GetLayerNum());
    analysis_manager->FillNtupleIColumn(1,1,hit->GetFiberNum());
    analysis_manager->FillNtupleDColumn(1,2,hit->GetPosition().x()/mm);
    analysis_manager->FillNtupleDColumn(1,3,hit->GetPosition().y()/mm);
    analysis_manager->FillNtupleDColumn(1,4,hit->GetPosition().z()/mm);
    analysis_manager->FillNtupleDColumn(1,5,hit->GetEnergyDep()/MeV);
    analysis_manager->FillNtupleDColumn(1,6,hit->GetProtonEnergyDep()/MeV);

    analysis_manager->AddNtupleRow(1);
}

// Fill scintillator ntuple with a collection
void AntiAnalyser::FillNtuple(ScintHitsCollection* hits_collection)
{
    size_t nhits = hits_collection->GetSize();
    for (unsigned long i = 0; i < nhits; ++i) {
        ScintHit* hit = static_cast<ScintHit*>(hits_collection->GetHit(i));
        FillNtuple(hit);
    }
}
