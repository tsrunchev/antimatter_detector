#include "RunAction.hh"
#include "G4Run.hh"

RunAction::RunAction(AntiAnalyser* analysis_manager)
: G4UserRunAction()
{
  analysisManager = analysis_manager;
}

RunAction::~RunAction()
{}

void RunAction::BeginOfRunAction(const G4Run* aRun)
{ 
  G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;
  
  //histograms
  //
  analysisManager->Create(); 
}

void RunAction::EndOfRunAction(const G4Run* aRun)
{
  analysisManager->Save();
}