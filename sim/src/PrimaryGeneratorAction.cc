#include "PrimaryGeneratorAction.hh"
#include "Dimensions.hh"

#include <G4ParticleTable.hh>
#include <G4Event.hh>
#include <G4ParticleGun.hh>
#include <G4GeneralParticleSource.hh>
#include <G4Proton.hh>

#include <iostream>

using namespace std;

PrimaryGeneratorAction::PrimaryGeneratorAction()
{

    fGPS = new G4GeneralParticleSource();
    // fGPS->SetParticleDefinition(G4AntiProton::AntiProtonDefinition());
    fGPS->SetParticleDefinition(G4Proton::ProtonDefinition());

    G4SingleParticleSource* source = fGPS->GetCurrentSource();
    G4SPSPosDistribution* posDist = source->GetPosDist();
    posDist->SetCentreCoords(G4ThreeVector(0., 0., dimensions::source_z));
    // posDist->Set

}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
    // delete fGun;
    delete fGPS;
    // myFile.close();
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
    // Position randomization
    // G4double x0 = 0,  y0  = 0 , z0  = 136.06*mm;
    // G4double dx0 = 210*mm, dy0 = 297*mm, dz0 = 0 * cm;
    // x0 += dx0 * (G4UniformRand() - 0.5);
    // y0 += dy0 * (G4UniformRand() - 0.5);
    // z0 += dz0 * (G4UniformRand() - 0.5);
    // G4double momentum = (50 + G4UniformRand()*1950)*MeV;
    // auto mom_direction = G4ThreeVector(
    //     (G4UniformRand()-0.5)*2,
    //     (G4UniformRand()-0.5)*2,
    //     -G4UniformRand());

    // G4double momentum = (50 + G4UniformRand()*1950)*MeV;
    // auto mom_direction = G4ThreeVector(0,0,-1);
    //     (G4UniformRand()-0.5)*2,
    //     (G4UniformRand()-0.5)*2,
    //     -G4UniformRand());
    // auto mom_direction = fGun->GetParticleMomentumDirection();
    // myFile<<x0<<","<<y0<<","<<z0<<","<<
    //     fGun->GetParticleMomentum()/MeV<<","<<
    //     mom_direction.getX()<<","<<
    //     mom_direction.getY()<<","<<
    //     mom_direction.getZ()<<endl;

    // fGun->SetParticlePosition({x0, y0, z0});
    // fGun->SetParticleMomentum(mom_direction);
    // fGun->SetParticleMomentumDirection(mom_direction);
    
    // fGun->GeneratePrimaryVertex(anEvent);
    fGPS->GeneratePrimaryVertex(anEvent);
}
