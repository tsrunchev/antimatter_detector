#include "ActionInitialization.hh"
#include "PrimaryGunAction.hh"
#include "AntiAnalyser.hh"
#include "RunAction.hh"
#include "EventAction.hh"

#include "DetectorConstruction.hh"

ActionInitialization::ActionInitialization(AntiAnalyser* run_analyser, DetectorConstruction* detector)
  : fRunAnalyser{run_analyser}, fDetector{detector}
  {}

ActionInitialization::~ActionInitialization()
{
  delete fRunAnalyser;
}

void ActionInitialization::Build() const
{
    // SetUserAction(new PrimaryGunAction());
    SetUserAction(new PrimaryGunAction(fDetector));

    SetUserAction(new RunAction(fRunAnalyser));
    SetUserAction(new EventAction(fRunAnalyser));
}

void ActionInitialization::BuildForMaster() const
{
  // By default, don't do anything. This applies only in MT mode:
  // SetUserAction(new RunAction());
}
