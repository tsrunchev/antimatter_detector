import os
import re

def get_next_name(filepath, digits=2):
    """ Numbers similarly named files depending on what is already at destination
    
    Parameters
    ----------
        filepath : string
        digits : int, optional
            number of digits to append, by default 2
    """

    folder, filename = os.path.split(filepath)
    root, ext = os.path.splitext(filename)

    if not folder:
        folder = '.'

    pattern = re.compile(f'{root}_(\d{{{digits}}}){ext}')
    list_of_files = [os.path.join(f'{folder}',f'{i}') for i in os.listdir(os.path.abspath(folder)) if pattern.match(i)]
    last_num = 0
    if list_of_files:
        last_num = int(os.path.splitext(max(list_of_files, key=os.path.getctime))[0][-digits:])

    return os.path.join(f'{folder}',f'{root}_{last_num+1:0{digits}d}{ext}')
