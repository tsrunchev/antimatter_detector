import numpy as np
from matplotlib import pyplot as plt

from my_tools import get_next_name

def plot_real_vs_reconstructed_radius(momenta,rec_radii,real_rec_rad,path='../python_data/all'):
    """ Plots reconstructed and real (reconstructed) radii vs transverse momenta with and without a linear fit """
    p,res,_,_,_ = np.polyfit(momenta,rec_radii,1,full=True)
    p2,res2,_,_,_ = np.polyfit(momenta,real_rec_rad,1,full=True)

    # Without fit
    fig,ax = plt.subplots(2,figsize=(8,6))
    ax.plot(momenta,rec_radii,'o',label='reconstructed radius')
    ax.plot(momenta,real_rec_rad,'o', label='real radius')
    ax.legend()
    ax.set_title('Reconstructed gyration radius vs transverse momentum')
    ax.set_xlabel('Transverse momentum before magnetic field [MeV/c]')
    ax.set_ylabel('Reconstructed gyration radius [mm]')
    fig.savefig(get_next_name(f'{path}r(p).png'))

    # Adding fit
    chi2_1=np.sum((np.polyval(p, momenta) - rec_radii) ** 2/np.polyval(p, momenta))/(momenta.shape[0]-2)
    chi2_2=np.sum((np.polyval(p2, momenta) - real_rec_rad) ** 2/np.polyval(p2, momenta))/(momenta.shape[0]-2)
    ax.plot([momenta.min(),momenta.max()],np.polyval(p,[momenta.min(),momenta.max()]),
        label=f'reconstructed $\chi^2/ndof=${chi2_1}')
    ax.plot([momenta.min(),momenta.max()],np.polyval(p2,[momenta.min(),momenta.max()]),
        label=f'real $\chi^2/ndof=${chi2_2}')
    ax.legend()
    fig.savefig(get_next_name(f'{path}r(p)_fitted.png'))
    
    fig.clf()