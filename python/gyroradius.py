# import collections
import json
import os
import re

import matplotlib.pyplot as plt
from natsort import natsorted
import numpy as np
# import pandas as pd

from my_tools import get_next_name

def reject_outliers(data, m=3):
    """ Reject elements more than m times the standard deviation away from the median """
    return abs(data - np.median(data)) < (m * np.std(data))


def smear(single_data, sigma=0.01):
    """ Gaussian smear x and y dimensions of series of 3D points """
    return np.array([(np.random.normal(x,sigma,1)[0],
            np.random.normal(y,sigma,1)[0],
            z) for (x,y,z) in single_data])
    

def gyroradius2d(tang_2dvect_1, tang_2dvect_2, pnt2d_1, pnt2d_2):
    """ Calculate radius of a circle from two 2D tangent vectors and their starting points """
    # Get orthogonal vectors
    o1 = np.array([tang_2dvect_1[1],-tang_2dvect_1[0]])
    o2 = np.array([tang_2dvect_2[1],-tang_2dvect_2[0]])

    # Solve linear equation for points where vectors cross
    # (how many times to I need to add o1 or o2 to points 1 and 2 for them to cross)
    a = np.array([o1,-o2]).transpose()
    b = pnt2d_2-pnt2d_1
    s = np.linalg.solve(a,b)

    # Get point
    centre1 = o1*s[0] + pnt2d_1
    
    # alternatively this should return same
    # centre2 = o2*s[1] + pnt2d_2
    # return np.linalg.norm(pnt2d_2-centre2)

    return np.linalg.norm(pnt2d_1-centre1)


def gyroradius(pts_3D,plane=[0,1]):
    """ Get xy 2D tangent vectors and points from 3D points (CMOS hits) and call gyroradius2D """
    r1 = pts_3D[1]-pts_3D[0]
    r2 = pts_3D[3]-pts_3D[2]
    r1 = r1[plane]
    r2 = r2[plane]
    return gyroradius2d(r1,r2,pts_3D[1][:2],pts_3D[2][:2])



def analysis(data, do_smear=True, smear_sigma=0.01):
    """
    Takes CMOS detector hit data, apply gaussian smear to plane coordinates, 
    calculate total transverse momentum, real gyroradius using momentum vectors 
    before and after the magnetic field and reconstructed radius using 3d hit points.
    Data comes as numpy array containing quartets of rows with columns\n
        0 int CMOSLayer
        1 double PosX[mm]
        2 double PosY[mm]
        3 double PosZ[mm]
        4 double PreMomX[MeV/c]
        5 double PreMomY[MeV/c]
        6 double PreMomZ[MeV/c]
        7 double PostMomX[MeV/c]
        8 double PostMomY[MeV/c]
        9 double PostMomZ[MeV/c]
    and returns 2 pandas.Dataframe objects with columns\n
        ['smeared_x','smeared_y','z']
        ['transverse_momenta','reconstructed_radii','real_radii']
        # transverse_momenta -- transverse momentum entering the magnetic field
        # reconstructed_radii -- reconstructed radii from smeared 2D coordinates
        # real_radii -- reconstructed radii from transverse momenta just before and after the magnetic field
    """

    # these can be function parameters if data changes a lot (I don't think it will)
    CMOSLayer = 0
    PositionIndex = np.arange(1,4)
    PreMomentumIndex = np.arange(4,7)
    PostMomentumIndex = np.arange(7,10)
    # [0,1] means xy
    plane = np.arange(2)
    
    smeared_coords = np.zeros((data.shape[0],3))
    transverse_momenta = np.zeros(data.shape[0]//4)
    reconstructed_radii = np.zeros(data.shape[0]//4)
    real_radii = np.zeros(data.shape[0]//4)

    # Go through events, each with 4 hits
    for i in range(data.shape[0]//4):
        # Get coordinates
        event_coords = data[i*4:(i+1)*4, PositionIndex]
        if do_smear:
            event_coords = smear(event_coords,smear_sigma)
        smeared_coords[i*4:(i+1)*4] = event_coords

        transverse_momenta[i] = np.sqrt(np.sum(data[i*4+1, PostMomentumIndex[plane]]**2))
        try:
            reconstructed_radii[i] = gyroradius(event_coords)
        except:
            reconstructed_radii[i] = 0
        
        # Get transverse momentum for real radius calculation
        # cmos labels 0-1-magnetic_field-2-3
        premag_mom_2d = data[i*4+1,PostMomentumIndex[plane]]
        premag_pos_2d = data[i*4+1,PositionIndex[plane]]
        postmag_mom_2d = data[i*4+2,PreMomentumIndex[plane]]
        postmag_pos_2d = data[i*4+2,PositionIndex[plane]]
        try:
            real_radii[i] = gyroradius2d(premag_mom_2d,postmag_mom_2d,premag_pos_2d,postmag_pos_2d)
        except:
            # assume it was a division by zero
            real_radii[i] = 0
    return smeared_coords,transverse_momenta,reconstructed_radii,real_radii
    # return (pd.DataFrame(smeared_coords,columns=['smeared_x','smeared_y','z']),
    #         pd.DataFrame({"transverse_momenta":transverse_momenta,
    #                       "reconstructed_radii":reconstructed_radii,
    #                       "real_radii":real_radii}))

def analyse_folder(folder, sigma, to_smear, file_selector=lambda f: f):
    """ Gets all the files in the folder and tries to analyse them one by one """
    files = natsorted([f for f in os.listdir(folder) if os.path.isfile(os.path.join(folder,f))])

    _,transverse_momenta,reconstructed_radii,real_radii = \
        analysis(np.genfromtxt(os.path.join(folder,files[0]),
                               skip_header=14,delimiter=','),sigma,to_smear)
    for f in files[1:]:
        data = np.genfromtxt(os.path.join(folder,f), skip_header=14,delimiter=',')
        single_coordinates, single_momenta,single_radii, single_radii_real = analysis(data,sigma,to_smear)
        
        transverse_momenta = np.concatenate((transverse_momenta,single_momenta),axis=0)
        reconstructed_radii = np.concatenate((reconstructed_radii,single_radii),axis=0)
        real_radii = np.concatenate((real_radii,single_radii_real),axis=0)

    return transverse_momenta,reconstructed_radii,real_radii

def repeated_analysis_folder(folder, nsmear=1000, save_path='../python_data/temp'):
    files = natsorted([f for f in os.listdir(folder) if 
                       os.path.isfile(os.path.join(folder,f)) 
                       and f[-8:-4]=='CMOS'])
    
    if not os.path.exists(save_path):
        os.mkdir(save_path)
    
    file_data = np.zeros(())
    for f in files:
        data = np.genfromtxt(os.path.join(folder,f), skip_header=14,delimiter=',')
        _,momenta,radii,radii_real = analysis(data)
        for i in range(nsmear-1):
            _, single_momenta,single_radii, single_radii_real = analysis(data)
            
            momenta = np.concatenate((momenta,single_momenta),axis=0)
            radii = np.concatenate((radii,single_radii),axis=0)
            radii_real = np.concatenate((radii_real,single_radii_real),axis=0)
        
        np.savetxt(get_next_name(os.path.join(save_path,f'{f}_gyro')),
                   np.stack([momenta,radii,radii_real],axis=1),
                   header='transverse_momentum,reconstructed_radius,real_radius')
                    
        # np.savetxt(get_next_name(os.path.join(save_path,f'{f}_smeared_coords')),
        #                          smeared_coordinates,header='x,y,z')
    
    return momenta,radii,radii_real

# def smear_analyse_folder(folder, nsmear=1000, file_selector=lambda x: x, do_save_each=True,
#                          save_path='../python_data/temp'):
#     """
#     Runs smearing analysis on each file in *folder* *nsmear* times. Files can further be filtered
#     with the file_selector.
#         - save_path folder is created if it doesn't exist. Each file's smeared results can be saved here.
#     """
#     files = natsorted([f for f in os.listdir(folder) if 
#                        os.path.isfile(os.path.join(folder,f)) and file_selector(f)])
    
#     if do_save_each and not os.path.exists(save_path):
#         os.mkdir(save_path)
    
#     output_data = pd.DataFrame(columns=['transverse_momenta','reconstructed_radii','real_radii'])
#     smeared_coordinates_df = pd.DataFrame(columns=['smeared_x','smeared_y','z'])
#     for i,f in enumerate(files):
#         data = np.genfromtxt(os.path.join(folder,f), skip_header=14,delimiter=',')
#         smears_per_file_df = pd.DataFrame(columns=['transverse_momenta','reconstructed_radii','real_radii'])
#         smeared_coords_per_file_df = pd.DataFrame(columns=['smeared_x','smeared_y','z'])
        
#         for i in range(1000):
#             single_coords, single_gyrodata = analysis(data,do_smear=True)
#             smears_per_file_df = smears_per_file_df.append(single_gyrodata,ignore_index=True)
#             smeared_coords_per_file_df = smeared_coords_per_file_df.append(single_coords,ignore_index=True)
#         if do_save_each:
#             smears_per_file_df.to_csv(get_next_name(os.path.join(save_path,f'smeared_{f}')),index=False)
#         output_data = output_data.append(smears_per_file_df,ignore_index=True)
    
#     return output_data