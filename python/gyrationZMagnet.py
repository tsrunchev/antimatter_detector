import csv
import numpy as np
import matplotlib.pyplot as plt
import os
import re
import json
from natsort import natsorted
import pandas as pd

from my_tools import get_next_name

PATH = ''
CWD = os.getcwd()
if CWD[-6:]!='python':
    PATH = r'python/'
SAVEPATH = f'{PATH}gyroradius_output/cmosSpacing/'


def analyse_folder(folder, sigma, to_smear, doRejectOutliers):
    """ Gets all the files in the folder and tries to analyse them one by one """
    files = natsorted([f for f in os.listdir(folder) if os.path.isfile(f'{folder}{f}')])

    first = True
    for f in files:
        data = np.genfromtxt(f'{folder}{f}', skip_header=14,delimiter=',')
        single_radii, single_radii_real, single_coordinates, single_momenta = analysis(data,sigma,to_smear)
        if first:
            momenta = single_momenta
            radii = single_radii
            radii_real   = single_radii_real
            first = False
        else: 
            momenta = np.concatenate((momenta,single_momenta),axis=0)
            radii = np.concatenate((radii,single_radii),axis=0)
            radii_real = np.concatenate((radii_real,single_radii_real),axis=0)

    # std_1 = np.std(y)
    # std_2 = np.std(y2)
    # med_1 = np.median(y)
    # med_2 = np.median(y2)
    # med_1,std_1,med_2,std_2

    if doRejectOutliers:
        reject1 = reject_outliers(radii,3)
        reject2 = reject_outliers(radii_real,3)
        reject3 = radii_real>0
        radii = radii[reject1&reject2&reject3]
        radii_real = radii_real[reject1&reject2&reject3]
        momenta = momenta[reject1&reject2&reject3]

    return momenta,radii,radii_real


def fit_plot(momenta,rec_radii,real_rec_rad,to_smear,name='all'):
    """ Plots reconstructed and real (reconstructed) radii vs transverse momenta with and without a linear fit """
    p,res,_,_,_ = np.polyfit(momenta,rec_radii,1,full=True)
    p2,res2,_,_,_ = np.polyfit(momenta,real_rec_rad,1,full=True)

    # Without fit
    plt.plot(momenta,rec_radii,'o',label='reconstructed radius')
    plt.plot(momenta,real_rec_rad,'o', label='real radius')
    plt.legend()
    plt.title('Reconstructed gyration radius vs transverse momentum')
    plt.xlabel('Transverse momentum before magnetic field [MeV/c]')
    plt.ylabel('Reconstructed gyration radius [mm]')
    plt.savefig(get_next_name(f'{SAVEPATH}{name}r(p).png'))

    # Adding fit
    chi2_1=np.sum((np.polyval(p, momenta) - rec_radii) ** 2/np.polyval(p, momenta))/(momenta.shape[0]-2)
    chi2_2=np.sum((np.polyval(p2, momenta) - real_rec_rad) ** 2/np.polyval(p2, momenta))/(momenta.shape[0]-2)
    plt.plot([momenta.min(),momenta.max()],np.polyval(p,[momenta.min(),momenta.max()]),
        label=f'reconstructed $\chi^2/ndof=${chi2_1}')
    plt.plot([momenta.min(),momenta.max()],np.polyval(p2,[momenta.min(),momenta.max()]),
        label=f'real $\chi^2/ndof=${chi2_2}')
    plt.legend()
    plt.savefig(get_next_name(f'{SAVEPATH}{name}r(p)_fitted.png'))
    plt.show()
    plt.clf()

    rp_filename = f'{name}Smearedr.csv' if to_smear else f'{name}nonSmearedr.csv'
    # TODO: Add header row and adjust notebooks/analysis scripts to handle it if necessary
    np.savetxt(get_next_name(f'{SAVEPATH}{rp_filename}'),np.stack([momenta,rec_radii], axis=1))



def reject_outliers(data, m=3):
    """ Reject elements more than m times the standard deviation away from the median """
    return abs(data - np.median(data)) < (m * np.std(data))


def smear2(single_data, sigma=0.01):
    """ Gaussian smear x and y dimensions of series of 3D points """
    return np.array([(np.random.normal(x,sigma,1)[0],
            np.random.normal(y,sigma,1)[0],
            z) for (x,y,z) in single_data])

def gyroradius2d(tang_2dvect_1, tang_2dvect_2, pnt2d_1, pnt2d_2):
    """ Calculate radius of a circle from two 2D tangent vectors and their starting points """
    # Get orthogonal vectors
    o1 = np.array([tang_2dvect_1[1],-tang_2dvect_1[0]])
    o2 = np.array([tang_2dvect_2[1],-tang_2dvect_2[0]])

    # Solve linear equation for points where vectors cross
    # (how many times to I need to add o1 or o2 to points 1 and 2 for them to cross)
    a = np.array([o1,-o2]).transpose()
    b = pnt2d_2-pnt2d_1
    s = np.linalg.solve(a,b)

    # Get point
    centre1 = o1*s[0] + pnt2d_1
    
    # alternatively this should return same
    # centre2 = o2*s[1] + pnt2d_2
    # return np.linalg.norm(pnt2d_2-centre2)

    return np.linalg.norm(pnt2d_1-centre1)



def gyroradius(pts_3D):
    """ Get xy 2D tangent vectors and points from 3D points and call gyroradius2D """
    r1 = pts_3D[1]-pts_3D[0]
    r2 = pts_3D[3]-pts_3D[2]
    r1 = r1[:2]
    r2 = r2[:2]
    return gyroradius2d(r1,r2,pts_3D[1][:2],pts_3D[2][:2])



def analysis(data, sigma=0.01, to_smear=True):
    """
    Takes data as numpy array containing quartets of rows with columns\n
        0 int CMOSLayer
        1 double PosX[mm]
        2 double PosY[mm]
        3 double PosZ[mm]
        4 double PreMomX[MeV/c]
        5 double PreMomY[MeV/c]
        6 double PreMomZ[MeV/c]
        7 double PostMomX[MeV/c]
        8 double PostMomY[MeV/c]
        9 double PostMomZ[MeV/c]
    and returns 4 numpy arrays\n
        radii -- reconstructed radii from smeared 2D coordinates
        radii_true -- reconstructed radii from transverse momenta just before and after the magnetic field
        smeared_coords -- 3D input coordinates with XY smeared by sigma [mm]
        momenta -- transverse momentum entering the magnetic field
    """

    # these can be function parameters if data changes a lot (I don't think it will)
    CMOSLayer = 0
    PosInd = np.arange(1,4)
    PreMomInd = np.arange(4,7)
    PostMomInd = np.arange(7,10)
    plane = np.arange(2)
    
    radii = np.zeros(data.shape[0]//4)
    radii_true = np.zeros(data.shape[0]//4)
    smeared_coords = np.zeros((data.shape[0]//4,4,3))
    momenta = np.zeros(data.shape[0]//4)

    # Go through events, each with 4 hits
    for i in range(data.shape[0]//4):
        event_coords = data[i*4:(i+1)*4, PosInd]
        if to_smear:
            event_coords = smear2(event_coords,sigma)
        smeared_coords[i] = event_coords

        momenta[i] = np.sqrt(np.sum(data[i*4+1, PostMomInd[plane]]**2))
        radii[i] = gyroradius(smeared_coords[i])
        try:
            premag_mom_2d = data[i*4+1,PostMomInd[plane]]
            premag_pos_2d = data[i*4+1,PosInd[plane]]
            postmag_mom_2d = data[i*4+2,PreMomInd[plane]]
            postmag_pos_2d = data[i*4+2,PosInd[plane]]
            radii_true[i] = gyroradius2d(premag_mom_2d,postmag_mom_2d,premag_pos_2d,postmag_pos_2d)
        except:
            radii_true[i] = 0
    return radii, radii_true, smeared_coords, momenta

# if __name__ == "__main__":
#     sigma = 0.01 #mm
#     to_smear = True
#     # folder = r'../archive/ntuples_Bz_10cmL
#     folder = rf'{path}../ntuple/gyroFeb/'
#     doRejectOutliers = True
#     momenta,radii,radii_real = analyse_folder(folder, sigma, to_smear, doRejectOutliers)
#     fit_plot(momenta,radii,radii_real,to_smear)